package net.stopi.headmovementtracker;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import jp.epson.moverio.bt200.SensorControl;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

	private SensorManager mSensorManager;
	private SensorFusion mSensorFusion;
	private Sensor mGyroscope, mAccelerometer, mMagneticField, mAcceleration, mRotationVector;
	private DrawingSurfaceView mSurfaceView;
	private float azimuth, pitch, roll;
	private boolean isFirstRun = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SensorControl sc = new SensorControl(this);
		sc.setMode(SensorControl.SENSOR_MODE_HEADSET);

		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		mSensorFusion = new SensorFusion();
		mSensorFusion.setMode(SensorFusion.Mode.FUSION);
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		initSensors();
		registerListeners(SensorManager.SENSOR_DELAY_UI);

		mSurfaceView = (DrawingSurfaceView) findViewById(R.id.surfaceView);
	}

	private void initSensors()
	{
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		mMagneticField = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		mAcceleration = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
		mRotationVector = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
	}

	private void registerListeners(int sensingPeriods)
	{
		mSensorManager.registerListener(this, mAccelerometer, sensingPeriods);
		mSensorManager.registerListener(this, mMagneticField, sensingPeriods);
		mSensorManager.registerListener(this, mGyroscope, sensingPeriods);
		mSensorManager.registerListener(this, mAcceleration, sensingPeriods);
		mSensorManager.registerListener(this, mRotationVector, sensingPeriods);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSensorChanged(SensorEvent sensorEvent) {
		switch (sensorEvent.sensor.getType())
		{
			/*case Sensor.TYPE_ACCELEROMETER:
				mSensorFusion.setAccel(sensorEvent.values);
				mSensorFusion.calculateAccMagOrientation();
				break;
			case Sensor.TYPE_MAGNETIC_FIELD:
				mSensorFusion.setMagnet(sensorEvent.values);
				break;*/
			case Sensor.TYPE_ROTATION_VECTOR:
				mSensorFusion.setRotationVector(sensorEvent.values);
				mSensorFusion.calculateAccMagOrientationFromVector();

				float[] o, r;
				o = new float[3];
				r = new float[9];

				SensorManager.getRotationMatrixFromVector(r, sensorEvent.values);
				SensorManager.getOrientation(r, o);

				mSurfaceView.setAzimuth((float) (o[2] * 180 / Math.PI) + 180);
				mSurfaceView.setPitch((float) (o[1] * 180 / Math.PI) + 90);
				mSurfaceView.setRoll((float) (o[0] * 180 / Math.PI) + 180);

				break;
			case Sensor.TYPE_GYROSCOPE:
				//mSensorFusion.gyroFunction(sensorEvent);
				break;
			case Sensor.TYPE_LINEAR_ACCELERATION:
				//Log.d("LinearAcceleration", "x = " + sensorEvent.values[0] + " | y = " + sensorEvent.values[1] + " | z = " + sensorEvent.values[2]);
				break;
		}

		/*mSurfaceView.setAzimuth((float) mSensorFusion.getAzimuth());
		mSurfaceView.setPitch((float) mSensorFusion.getPitch());
		mSurfaceView.setRoll((float) mSensorFusion.getRoll());*/

		if(isFirstRun)
		{
			mSurfaceView.setRectangleCoords();
			isFirstRun = false;
		}

		Log.d("SensorFusion", "Azimuth = " + mSurfaceView.getAzimuth() + " | Roll = " + mSurfaceView.getRoll() + " | Pitch = " + mSurfaceView.getPitch());

		mSurfaceView.invalidate();
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int i) {

	}
}
