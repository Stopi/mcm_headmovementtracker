package net.stopi.headmovementtracker;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class DrawingSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

	public static int PORT = 8889;
	private DrawingThread mThread = null;
	private float azimuth = 0;
	private float pitch = 0;
	private float roll = 0;
	private float left = 320, top = 180, right = 640, bottom = 360;
	private float rectAzimuth, rectPitch, rectRoll;
	private Paint p, textPaint;

	public DrawingSurfaceView(Context context) {
		super(context);
		init();
	}

	public DrawingSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);

		init();
	}

	public DrawingSurfaceView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		init();
	}

	private void init() {
		System.out.println("[DEBUG] SurfaceView created");
		setWillNotDraw(false);
		getHolder().addCallback(this);
	}

	public DrawingThread getThread() {
		return mThread;
	}

	public void onResumeDrawingSurfaceView() {
		mThread = new DrawingThread(getHolder());
		mThread.setRunning(true);
		mThread.start();
	}

	public void onPauseDrawingSurfaceView() {
		boolean retry = true;
		mThread.setRunning(false);
		while (retry) {
			try {
				mThread.join();
				retry = false;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public double calculateDistance(Point p1, Point p2) {
		double a = Math.abs(p1.x - p2.x);
		double b = Math.abs(p1.y - p2.y);
		return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
	}

	public ArrayList<Point> convertJsonToPoints(String json) {
		System.out.println("[DEBUG] JsonToPoints convert started");

		ArrayList<Point> points = new ArrayList<Point>();

		if (json.contains(",")) {
			String[] pointPairs = json.split(",");
			for (String pair : pointPairs) {
				points.add(convertCoordPairToPoint(pair));
			}
		} else {
			points.add(convertCoordPairToPoint(json));
		}

		System.out.println("[DEBUG] JsonToPoints convert finished");

		return points;
	}

	private Point convertCoordPairToPoint(String pair) {
		String[] xyPair = pair.split(";");

		int xCoord = Integer.parseInt(xyPair[0].split("=")[1]);
		int yCoord = Integer.parseInt(xyPair[1].split("=")[1]);

		return new Point(xCoord, yCoord);
	}

	public void surfaceCreated(SurfaceHolder holder) {
		Canvas c = holder.lockCanvas();
		p = new Paint();
		p.setStrokeWidth(5f);
		p.setColor(Color.RED);

		textPaint = new Paint();
		textPaint.setTextAlign(Paint.Align.RIGHT);
		textPaint.setTextSize(20);
		textPaint.setColor(Color.GREEN);
		float tmpLeft = 0, tmpTop = 0, tmpRight = 0, tmpBottom = 0;

		tmpLeft = left + azimuth;
		tmpRight = right + azimuth;
		tmpTop = top + roll;
		tmpBottom = bottom + roll;

		c.drawColor(Color.BLACK);
		c.drawRect(tmpLeft, tmpTop, tmpRight, tmpBottom, p);

		holder.unlockCanvasAndPost(c);

		onResumeDrawingSurfaceView();
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		onPauseDrawingSurfaceView();
	}

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	public float getAzimuth() {
		return azimuth;
	}

	public void setAzimuth(float azimuth) {
		this.azimuth = azimuth;
	}

	public float getRoll() {
		return roll;
	}

	public void setRoll(float roll) {
		this.roll = roll;
	}

	public void setRectangleCoords()
	{
		rectAzimuth = getAzimuth();
		rectPitch = getPitch();
		rectRoll = getRoll();

		float factor = 3;

		// 180, 50 coords
		left = -540 + (320 / 2);//left + (getAzimuth() * factor);
		right = -540 + (320 / 2) + 320;//right + (getAzimuth() * factor);
		top = 150 + (180 / 2);//top + (getPitch() * -1 * factor);
		bottom = 150 + (180 / 2) + 180;//bottom + ( getPitch() * -1 * factor);
	}

	protected class DrawingThread extends Thread {

		private final SurfaceHolder mSurfaceHolder;
		private boolean running = false;
		private Paint mPaint;
		private Matrix mTransform;

		protected DrawingThread(SurfaceHolder holder) {
			mSurfaceHolder = holder;
			mPaint = new Paint();
			mPaint.setColor(Color.RED);
			mPaint.setStrokeWidth(5);
			mTransform = new Matrix();

			float translateX = 260 - (220);
			float translateY = 120 - (119);
			mTransform.setTranslate(translateX, translateY);
			mTransform.setScale(2, 2);
		}

		@Override
		public void run() {
			while (running) {
				if (mSurfaceHolder.getSurface().isValid()) {

						Canvas c = null;

						try {

							synchronized (mSurfaceHolder) {
								c = mSurfaceHolder.lockCanvas();
								if(c != null)
									doDraw(c);
							}
						} finally {

							if (c != null)
								mSurfaceHolder.unlockCanvasAndPost(c);

						}



				} else {
					System.out.println("[DEBUG] Surface is invalid");
				}
			}

		}

		public void doDraw(Canvas c)
		{
			float tmpLeft = 0, tmpTop = 0, tmpRight = 0, tmpBottom = 0, factor = 3, pointx = 1, pointy = 1;

			float azimuthFactor = (azimuth * -1 * factor);
			float pitchFactor = (pitch * factor);

			tmpLeft = 0 + azimuthFactor;
			tmpRight = 960 + azimuthFactor;
			tmpTop = 0 + pitchFactor;
			tmpBottom = 540 + pitchFactor;

			/*pointx += azimuth * factor;
			pointy += pitch * -1 * factor;

			c.drawPoint(pointx, pointy, p);*/

			//Log.d("Pos", "X = " + pointx + " | Y = " + pointy);

			c.drawColor(Color.BLACK);

			if(tmpLeft < left && tmpTop < top && tmpRight > right && tmpBottom > bottom)
				c.drawRect(left - azimuthFactor, top - pitchFactor, right - azimuthFactor, bottom - pitchFactor, p);

			textPaint.setTextAlign(Paint.Align.RIGHT);
			c.drawText("Left: " + tmpLeft, 840, 30, textPaint);
			c.drawText("Top: " + tmpTop, 840, 60, textPaint);

			textPaint.setTextAlign(Paint.Align.CENTER);
			c.drawText("AzFactor: " + azimuthFactor, 440, 30, textPaint);
			c.drawText("PiFactor: " + pitchFactor, 440, 60, textPaint);

			textPaint.setTextAlign(Paint.Align.LEFT);
			c.drawText("Azimuth: " + azimuth, 50, 30, textPaint);
			c.drawText("Pitch: " + pitch, 50, 60, textPaint);
			c.drawText("Roll: " + roll, 50, 90, textPaint);
			//Log.d("Factors", "Azimuth: " + azimuthFactor + " | Pitch: " + pitchFactor);
		}

		public boolean isRunning() {
			return running;
		}

		public void setRunning(boolean running) {
			this.running = running;
		}
	}
}
