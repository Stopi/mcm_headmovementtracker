package net.stopi.headmovementtracker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class RotationView extends View {

	private float azimuth;
	private Paint p;

	public RotationView(Context context) {
		super(context);

		p = new Paint();
		p.setColor(Color.RED);
		p.setStrokeWidth(2);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int viewWidth = getWidth();
		int viewHeight = getHeight();
		canvas.drawColor(Color.BLACK);

		canvas.drawRect(viewWidth/2-100, viewHeight/2+100, viewWidth/2+100, viewHeight/2-100, p);

		canvas.rotate(azimuth * -1 - 90);
	}

	public double getAzimuth() {
		return azimuth;
	}

	public void setAzimuth(float azimuth) {
		this.azimuth = azimuth;
		invalidate();
	}
}
